package model.data_structures;

public class Node {

	private Node siguiente;
	private Node anterior;
	private Object elemento;
	
	public Node(Object pElem) {
		elemento = pElem;
		anterior = null;
		siguiente = null;
	}
	
	public Object darElemento() {
		return elemento;
	}
	public Node darSig() {
		return siguiente;
	}
	public Node darAnt() {
		return anterior;
	}
	public void cambiarSig(Node pSig) {
		siguiente = pSig;
	}
	public void cambiarAnt(Node pAnt) {
		anterior = pAnt;
	}
	public void imprimirr() {
		System.out.println(elemento.toString());
	}
	
	
}
