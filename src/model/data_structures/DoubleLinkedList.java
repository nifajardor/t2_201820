package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList implements DoublyLinkedList{

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return tamanio;
	}
	private Node primero;
	private int tamanio;
	
	public DoubleLinkedList(Node pPrimero) {
		primero = pPrimero;
		tamanio = 0;
	}
	
	public Node darPrimero() {
		return primero;
	}
	public void anadirNodo(Node pAnadir) {
		primero.cambiarAnt(pAnadir);
		pAnadir.cambiarSig(primero);
		primero = pAnadir;
		tamanio++;
	}
	public void anadirNodoAlFinal(Node pAnadir, Node pExistente) {
		pExistente.cambiarSig(pAnadir);
		pAnadir.cambiarAnt(pExistente);
		tamanio++;
	}

}
