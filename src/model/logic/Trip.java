package model.logic;

public class Trip {

	public final static String MALE = "Male";
	public final static String FEMALE = "Female";
	public final static String NO_GENDER = "...";
	
	private String id;
	private String startTime;
	private String endTime;
	private String bikeId;
	private String tripDuration;
	private String startStationId;
	private String startStationName;
	private String endStationId;
	private String endStationName;
	private String userType;
	private String gender;
	private String birthyear;
	
	public Trip(String pId, String pStartTime, String pEndTime, String pBikeId, String pTripDuration, String pStartStationId,String pStartStationName, String pEndStationId, String pEndStationName, String pUserType, String pGender,String pBirthyear) {
		id = pId;
		startTime = pStartTime;
		endTime = pEndTime;
		bikeId = pBikeId;
		tripDuration = pTripDuration;
		startStationId = pStartStationId;
		startStationName = pStartStationName;
		endStationId = pEndStationId;
		endStationName = pEndStationName;
		userType=pUserType;
		gender = pGender;
		birthyear = pBirthyear;
	}
	/**
	 * Metodo para comparar
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "ID: "+id+", "+"Start Time: "+startTime+", "+"End Time: "+endTime+", "+"Bike id: "+bikeId+", "+"Trip Duration: "+tripDuration+", "+"Start Station id: "+startStationId+", "+"Start Station Name: "+startStationName+", "+"End Station id: "+endStationId+", "+"End Station Name: "+endStationName+", "+"User Type: "+userType+", "+"Gender: "+gender+", "+"Birthyear: "+birthyear;
	}
	
	public String elCoso() {
		return id+" "+tripDuration+" "+startStationName+" "+endStationName+'\n';
	}
	
	public String darId() {
		return id;
	}
	public String darStartTime() {
		return startTime;
	}
	public String darEndTime() {
		return endTime;
	}
	public String darBikeId() {
		return bikeId;
	}
	public String darTripDuration() {
		return tripDuration;
	}
	public String darStartStationId() {
		return startStationId;
	}
	public String darStartStationName() {
		return startStationName;
	}
	public String darEndStationId() {
	return endStationId;
	}
	public String darEndStationName() {
		return endStationName;
	}
	public String darUserType() {
		return userType;
	}
	public String darGender() {
		return gender;
	}
	public String darBirthYear() {
		return birthyear;
	}
	
}
