package model.logic;

import api.IDivvyTripsManager;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;
import java.io.BufferedReader;
import java.io.FileReader;
import model.data_structures.DoubleLinkedList;



public class DivvyTripsManager implements IDivvyTripsManager {


	private DoubleLinkedList trips;
	private DoublyLinkedList<VOTrip> tripsSrch;
	private DoubleLinkedList stations;

	public DoubleLinkedList darTrips() {
		return trips;
	}
	public DoubleLinkedList darStations() {
		return stations;
	}
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		BufferedReader lector = null;
		String linea = "";
		try {
			lector = new BufferedReader(new FileReader(stationsFile));
			linea = lector.readLine();
			linea = lector.readLine();
			int contador = 0;
			String[] info = linea.split(",");
			Node primero = new Node(new Station(info[0], info[1], info[2], info[3], info[4], info[5], info[6]));
			stations = new DoubleLinkedList(primero);
			linea = lector.readLine();
			while(linea != null) {
				contador++;
				info = linea.split(",");
				String p0 = info[0];
				String p1 = info[1];
				String p2 = info[2];
				String p3 = info[3];
				String p4 = info[4];
				String p5 = info[5];
				String p6 = info[6];

				Node agregar = new Node(new Station(p0,p1,p2,p3,p4,p5,p6));
				stations.anadirNodo(agregar);
//				System.out.println(agregar.darElemento().toString());
//				System.out.println(contador);
				linea = lector.readLine();
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		//recorrerListaTrips();

	}


	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		BufferedReader lector = null;
		String linea = "";
		try {
			lector = new BufferedReader(new FileReader(tripsFile));
			linea = lector.readLine();
			linea = lector.readLine();
			int contador = 0;
			String[] info = linea.split(",");
			Node primero = new Node(new Trip(info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info[9], info[10], info[11]));
			Node primeroSrch = new Node(new VOTrip(Integer.parseInt(info[0]), Double.parseDouble(info[4]), info[6], info[8]));
			trips = new DoubleLinkedList(primero);
			tripsSrch = new DoubleLinkedList(primeroSrch);
			linea = lector.readLine();
			while (linea  != null) {
				contador++;
				info = linea.split(",");
				String p0 = info[0];
				String p1 = info[1];
				String p2 = info[2];
				String p3 = info[3];
				String p4 = info[4];
				String p5 = info[5];
				String p6 = info[6];
				String p7 = info[7];
				String p8 = info[8];
				String p9 = info[9];
				String p10 = "...";
				String p11 = "";
				if(info.length>10) {
					p10 = info[10];
					if(info.length>11) {
						p11 = info[11];
					}
				}

				Node agregarSrch = new Node(new VOTrip(Integer.parseInt(p0), Double.parseDouble(p4), p6, p8));
				Node agregar = new Node(new Trip(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11));
				trips.anadirNodo(agregar);
				tripsSrch.anadirNodo(agregarSrch);
				System.out.println(contador);
				linea = lector.readLine();
			}
			lector.close();

		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub
		DoubleLinkedList aRetornar = null;

		if(gender.equals("1")) {
			gender = Trip.MALE;
		}
		else if(gender.equals("2")) {
			gender = Trip.FEMALE;
		}
		else if(gender.equals("3")) {
			gender = Trip.NO_GENDER;
		}
		
		aRetornar = agregarAListaTripsPorCriterio(aRetornar, "Gender: ", gender);


		return aRetornar;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		DoubleLinkedList aRetornar = null;

		
		aRetornar = agregarAListaTripsPorCriterio(aRetornar, "End Station id: ", stationID + ",");

		return aRetornar;

	}	


	public DoubleLinkedList agregarAListaTripsPorCriterio(DoubleLinkedList pLista, String pAtributo, String pValor) {
		Node actual = trips.darPrimero();
		Node actualSrch = tripsSrch.darPrimero();
		String cadena = pAtributo + pValor;
		int contador = 0;
		Node existente = null;
		while(actual != null) {
			
			if(actual.darElemento().toString().contains(cadena)) {
				if(pLista == null) {
					pLista = new DoubleLinkedList(actualSrch);
				}
				else {
					pLista.anadirNodoAlFinal(actualSrch, existente);
				}

			}
			existente = actualSrch;
			actual = actual.darSig();
			actualSrch = actualSrch.darSig();
		}
		return pLista;
	}
	@Override
	public void imprimirEstaciones() {
		Node actual = stations.darPrimero();
		while(actual != null) {
			System.out.println(actual.darElemento().toString());
			actual = actual.darSig();
		}
	}
	@Override
	public void imprimirViajes() {
		Node actual = trips.darPrimero();
		while(actual != null) {
			System.out.println(actual.darElemento().toString());
			actual = actual.darSig();
		}
	}

}
