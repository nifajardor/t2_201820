package controller;

import api.IDivvyTripsManager;
import model.data_structures.DoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.logic.NoListException;
import model.vo.VOTrip;



public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	
	public static void loadStations(boolean pPar, String pRuta) {
		String ruta = ((pPar == true)? pRuta : "data/Divvy_Stations_2017_Q3Q4.csv");
		manager.loadStations(ruta);
	}
	
	public static void loadTrips(boolean pPar, String pRuta) {
		String ruta = ((pPar == true)? pRuta : "data/Divvy_Trips_2017_Q4.csv");
		manager.loadTrips(ruta);
		
	}
		
	public static DoublyLinkedList <VOTrip> getTripsOfGender (String gender){
		
		return manager.getTripsOfGender(gender);
	}
	
	public static DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		
		return manager.getTripsToStation(stationID);
	}
	public static void printStat() {
		manager.imprimirEstaciones();
	}
	public static void printTrip() {
		manager.imprimirViajes();
	}
}
