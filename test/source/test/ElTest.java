package test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import api.IDivvyTripsManager;
import controller.Controller;
import model.data_structures.*;
import model.logic.*;
import model.vo.*;


public class ElTest {

	@Before
	public void setupEscenario1() {
		Controller.loadTrips(true, "data/testStations.csv");
		Controller.loadTrips(true, "data/testTrips.csv");
	}
	@Test
	public void testLoadStations() {
		Controller.printStat();
	}
	@Test
	public void testLoadTrips() {
		Controller.printTrip();
	}
	
}
